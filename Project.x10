import x10.array.*;
import x10.io.Console;
import x10.io.File;
import x10.util.StringBuilder;
import x10.util.CUDAUtilities;
import x10.compiler.*;

class Project {
	// Flags
	var enableWavefront : Boolean = true; // If disabled, use naive iteration method
	var enableParallel : Boolean = true; // Requires Wavefront iteration to be enabled
	var enableDelay : Boolean = false; // Add 1ms delay to entry computation
	var enablePlaces : Boolean = false; // Use Places
	var enableCUDA : Boolean = false; // Use CUDA
	var enableFlattenMatrix : Boolean = true;
	var delay : Long = 1; // Add 1ms delay to entry computation
	var intervalLength : Long = 50; // Number of entries computed by a thread (tweak to optimize parallism)
	var minLength : Long = 550; // Minimum sequence size at which we start using parallel
	var maxDivisions : Long = 0; // Maximum number of threads to create

	var A : Float; // Gap open penalty
	var B : Float; // Gap extend penalty
	var s1 : String; // Sequence 1
	var s2 : String; // Sequence 2
	transient var ScoreMatrix : Rail[Long];
	transient var longestlen : Float = 0; // Highest entry value
	transient var longestx : Long = 0; // x-coordinate entry for longest sequence
	transient var longesty : Long = 0; // y-coordinate entry for longest sequence
	
	public static def main(args:Rail[String]):void {
		val prog = new Project();
		prog.run(args);
	}
	
	public def run(args:Rail[String]) {
		var SeqFile1 : String;
		var SeqFile2 : String;
		var ScoreFile : String;
		// Process arguments
		if (args.size < 5) { // Defaults
			Console.OUT.println("Usage: Project [sequence 1] [sequence 2] [score matrix] [gap open] [gap extend]");
			return;
		} else {
			SeqFile1 = args(0);
			SeqFile2 = args(1);
			ScoreFile = args(2);
			A = Float.parse(args(3));
			B = Float.parse(args(4));
			if (args.size > 5) {
				// Options processor
				for (i in 5..(args.size -1)) {
					if (args(i).equals("-seq")) {
						enableParallel = false;
					}
					if (args(i).equals("-delay")) {
						enableDelay = true;
					}
					if (args(i).equals("-places")) {
						enablePlaces = true;
					}
					if (args(i).equals("-cuda")) {
						enableCUDA = true;
					}
					if (args(i).equals("-interval")) {
						intervalLength = Long.parse(args(i+1));
					}
					if (args(i).equals("-len")) {
						minLength = Long.parse(args(i+1));
					}
					if (args(i).equals("-divisions")) {
						maxDivisions = Long.parse(args(i+1));
					}
				}
			}
		}
	
		s1 = read(SeqFile1);
		s2 = read(SeqFile2);
		readScore(ScoreFile);
		
		Console.OUT.println("Input Length #1: " + s1.length());
		Console.OUT.println("Input Length #2: " + s2.length());
		buildmatrix();
		
		// Flush contents so output buffer is written before program exits
		Console.OUT.flush();
	}
	
	// Reads the DNA sequence from file (note: assumes Unix file, X10 has bug with handling windows newlines)
	public def read(fname:String) {
		val f = new File(fname);
		var s : String = "";
		for (line in f.lines()) {
			if (line.length() == 0 as Int || line.charAt(0 as Int) == '>') {
				continue;
			}

			// Only accept allowed alphabets
			for (j in 0..(line.length()-1)) {
				var linechar : Char = line.charAt(j as Int);
				var allowed : String = "ARNDCQEGHILKMFPSTWYVBZX";
				if (allowed.indexOf(linechar) >= 0) {
					s += linechar;
				}
			}
		}
		return s;
	}
	
	// Reads the scoring matrix file (note: assumes Unix file)
	public def readScore(fname:String) {
		val f = new File(fname);
		var i : Long = 0;
		ScoreMatrix = new Rail[Long](24*24);
		for (line in f.lines()) {
			if (line.indexOf("#") >= 0) { // It is a comment
				continue;
			}

			var j : Long = 0;
			if (i > 0) { // Exclude the header name
				for (part in line.split(" ")) {
					if (part.length() > 0) {
						if (j == 0) { // Exclude the column name
							j++;
							continue;
						}
						
						try {
							var num : Long = Long.parseLong(part);
							var index : Long = (i-1) * 24 + j-1;
							ScoreMatrix(index) = num;
							j++;
						} catch (e: x10.lang.NumberFormatException) {
						}
					}
				}
			}
			i++;
		}
	}
	
	// Prints out the matrix
	public def debugMatrix() {
		for (i in 0..(2)) {
			for (j in 0..(m+n-1)) {
				Console.OUT.print(matrix(i,j) + " ");
			}
			Console.OUT.println("");
		}
	}
	
	// Generates the resultant sequences and display statistics
	public def trackback(pointers:Rail[UByte], sizesOfVerticalGaps:Rail[UShort], sizesOfHorizontalGaps:Rail[UShort]) {
		var n : Long = s2.length() + 1;
		var maxLen : Long = s1.length() + s2.length();
		var reversed1 : Rail[Char] = new Rail[Char](maxLen);
		var reversed2 : Rail[Char] = new Rail[Char](maxLen);
		var reversed3 : Rail[Char] = new Rail[Char](maxLen);
		var len1 : Long = 0;
		var len2 : Long = 0;
		var len3 : Long = 0;
		var identity : Long = 0;
		var gaps : Long = 0;
		var c1 : Char;
		var c2 : Char;
		var i : Long= longestx;
		var j : Long= longesty;
		var k : Long = i * n;
				
		while (true) {
			if (pointers(k+j) == 2uy) { // Up
				var len : Long = sizesOfVerticalGaps(k+j);
				for (l in 0..(len-1)) {
					i--;
					reversed1(len1++) = s1.charAt(i as Int);
					reversed2(len2++) = '-';
					k -= n;
					gaps++;
				}
			} else if (pointers(k+j) == 1uy) { // Diagonal
				i--;
				j--;
				c1 = s1.charAt(i as Int);
				c2 = s2.charAt(j as Int);
				
				k -= n;
				reversed1(len1++) = c1;
				reversed2(len2++) = c2;
				if (c1 == c2) {
					identity++;
				}
			} else if (pointers(k+j) == 3uy) { // Left
				var len : Long = sizesOfHorizontalGaps(k+j);
				for (l in 0..(len-1)) {
					j--;
					reversed1(len1++) = '-';
					reversed2(len2++) = s2.charAt(j as Int);
					gaps++;
				}
			} else { // Stop
				break;
			}
		}
		
		var r1 : String = reverse(reversed1, len1);
		var r2 : String = reverse(reversed2, len2);
		var total : Long = len1;
		
		Console.OUT.println(String.format("Identity: %d/%d (%.2f%%)", [identity, total, (identity as Float/total*100) as Float]));
		Console.OUT.println(String.format("Gaps: %d/%d (%.2f%%)", [gaps, total, (gaps as Float/total*100) as Float]));
		Console.OUT.println(String.format("Score: %.2f", [longestlen, 1]));
		
		// Show sequences
		Console.OUT.println("1: " + r1);
		Console.OUT.println("2: " + r2);
	}
	
	// Find the string that is the reverse of the array
	public def reverse(input:Rail[Char], len:Long) {
		var result : String = "";
		for (i in 1..len) {
			var c : Char = input(len - i);
			result += c;
		}
		return result;
	}
	
	// Computes the matrix at entry (i,j)
	public def computeEntry(i: Long, j: Long) {
		var f : Float = 0f;
		var l : Long = n * i + j; // id of this entry
		var mn : Long = m + n;
		
		var temp1 : Float = g(j) - B; // Top (Gotoh)
		var temp2 : Float = 0f;
		if (enableFlattenMatrix){
			f = matrixflat(((i-1 + j-1)%3)*mn + (j-1)) + similarity(i,j); // Diagonal
			temp2 = matrixflat(((i-1 + j)%3)*mn + j) - A; // Top (Gotoh)
		} else {
			f = matrix(i-1,j-1) + similarity(i,j);
			temp2 = matrix(i-1,j) - A;
		}
		
		// Top
		if (temp1 > temp2) {
			g(j) = temp1;
			sizesOfVerticalGaps(l) = sizesOfVerticalGaps(l-n) + 1us;
		} else {
			g(j) = temp2;
		}
		
		// Left
		temp1 = h(i) - B;
		if (enableFlattenMatrix){
			temp2 = matrixflat(((i + j-1)%3)*mn + (j-1)) - A;
		} else {
			temp2 = matrix(i,j-1) - A;
		}
		
		if (temp1 > temp2) {
			h(i) = temp1;
			sizesOfHorizontalGaps(l) = sizesOfHorizontalGaps(l-1) + 1us;
		} else {
			h(i) = temp2;
		}
		
		// Find max among the values computed
		var result : Float = f;
		pointers(l) = 1uy;
		if (g(j) > result) {
			result = g(j);
			pointers(l) = 2uy;
		}
		if (h(i) > result) {
			result = h(i);
			pointers(l) = 3uy;
		}
		if (0 >= result) {
			result = 0;
			pointers(l) = 0uy;
		}
		
		if (enableFlattenMatrix){
			matrixflat(((i + j)%3)*mn + j) = result;
		} else {
			matrix(i,j) = result;
		}
		
		// Set the starting cell for traceback
		if (result > longestlen) {
			longestlen = result;
			longestx = i;
			longesty = j;
		}
		
		if (enableDelay) {
			x10.lang.System.sleep(delay);
		}
	}
	
	// Constants used by algorithm
	var m : Long;
	var n : Long;
	val negativeInfinity : Float = -1000000;
	
	// Algorithm output values
	transient var pointers : Rail[UByte];
	transient var sizesOfVerticalGaps : Rail[UShort];
	transient var sizesOfHorizontalGaps : Rail[UShort];
	
	// Shared computation values
	transient var h : Rail[Float];  // Max value on left
	transient var g : Rail[Float];  // Max value on top
	
	var matrix : Array_2[Float]; // Naive 2d
	transient var matrixflat : Rail[Float]; // Linear matrix
	transient var s1arr : Rail[Char];
	transient var s2arr : Rail[Char];
	
	// Builds the matrix
	public def buildmatrix() {
		val startTime = x10.util.Timer.nanoTime();
		m = s1.length() + 1;
		n = s2.length() + 1;
		
		// Init traceback
		pointers = new Rail[UByte](m * n);
		sizesOfVerticalGaps = new Rail[UShort](m * n);
		sizesOfHorizontalGaps = new Rail[UShort](m * n);
		for (i in 0..(m*n-1)) {
			sizesOfVerticalGaps(i) = 1us;
			sizesOfHorizontalGaps(i) = 1us;
		}
		
		// Init matrix
		if (enableFlattenMatrix) {
			matrixflat = new Rail[Float](3 * (m+n));
		} else {
			matrix = new Array_2[Float](m, n);
		}
		
		// Init gotoh maximums
		g = new Rail[Float](n);
		h = new Rail[Float](m);
		for (j in 0..(m-1)) {
			h(j) = negativeInfinity;
		}
		for (j in 0..(n-1)) {
			g(j) = negativeInfinity;
		}
		
		// Start computation
		if (enableCUDA) {
			//cudaCompute();
		} else if (enablePlaces) {
			placesCompute();
		} else { // Non-places method (default)		
			if (!enableWavefront) {		
				naiveIteration();
			} else {
				wavefrontIteration();
			}
		}
		
		// Show results
		val endTime = x10.util.Timer.nanoTime();
		Console.OUT.println("Build Complete. Time taken: " + ((endTime - startTime) as double)/1000000 + " ms");
		trackback(pointers, sizesOfVerticalGaps, sizesOfHorizontalGaps);
	}
	
	// Naive iteration as used in JAligner
	public def naiveIteration() {
		Console.OUT.println("Using Sequential method");
		for (i in 1..(m-1)) {
			for (j in 1..(n-1)) {
				computeEntry(i,j);
			}
		}
	}
	
	// Wavefront parallelism method
	public def wavefrontIteration() {
		var extraMessage : String = "";
		var s1len : Long = s1.length();
		var s2len : Long = s2.length();
		
		if (s1len < minLength && s2len < minLength) {
			enableParallel = false;
			extraMessage = " (sequence too short)";
		}
	
		Console.OUT.println("Using Wavefront method");
		if (enableParallel) {
			extraMessage = extraMessage + ", Intervals: " + intervalLength;
			if (maxDivisions > 0) {
				extraMessage = extraMessage + ", Divisions: " + maxDivisions;
			}
		}
		Console.OUT.println("Parallel: " + enableParallel + extraMessage);
		var iterations: Long = s1.length() + s2.length() - 1; // Number of possible diagonals
		
		// Iteration data
		var startx : Long = 0;
		var starty : Long = 0;
		var diagonallen : Long = 0;
		
		for (i in 0..(iterations-1)) {
			// Find the starting coord of the diagonal, as well as length of diagonal
			var endy : Long = 0;
			if (i < s1len){
				startx = i;
				starty = 0;
				endy = startx;
			} else {
				startx = s1len-1;
				starty = i - startx;
				endy = starty + startx;
			}	
			
			if (endy >= s2len){ // Matrix is not square
				endy = s2len-1;
			}
			diagonallen = endy - starty + 1;
			
			if (enableParallel) {
				computeDiagonalParallel(startx, starty, diagonallen);
			} else {
				computeRange(startx, starty, diagonallen, false);
			}
		}
	}
	
	// Normal method
	public def computeDiagonalParallel(startx : Long, starty : Long, diagonallen : Long) {
		// Divide the diagonal into intervals, each thread only handle its own interval
		var divisions : Long = Math.ceil(diagonallen as Float / intervalLength) as Int;
		if (maxDivisions > 0 && maxDivisions < divisions) { // Limit divisions
			divisions = maxDivisions;
		}
		
		var covered : Long = 0;
		var interval : Long = Math.floor(diagonallen as Float / divisions) as Int;
		var intervallen : Long = interval;
		if (interval > 1) {
			finish for (c in 0..(divisions-1)) {
				// Last thread
				if (c == divisions-1) {
					intervallen = diagonallen - covered;
				}
				// Create threads for other iterations
				computeRange(startx - covered, starty + covered, intervallen, true);
				covered += interval;
			}
		} else {
			// One interval, just do sequential mode
			computeRange(startx, starty, diagonallen, false);
		}
	}
	
	// Calculate entries along a diagonal
	public def computeRange(i: Long, j: Long, range: Long, isasync: Boolean) {
		if (isasync) {
			async {
				for (c in 0..(range-1)) {
					computeEntry(i-c+1,j+c+1);
				}
			}
		} else {
			for (c in 0..(range-1)) {
				computeEntry(i-c+1,j+c+1);
			}
		}
	}
	
	// Similarity function to compare two letters
	public def similarity(i:Long, j:Long) {
		var scoremap : String = "ARNDCQEGHILKMFPSTWYVBZX";
		var index1 : Long = scoremap.indexOf(s1.charAt((i-1) as Int));
		var index2 : Long = scoremap.indexOf(s2.charAt((j-1) as Int));
		
		// Wildcard entry
		if (index1 < 0) {
			index1 = scoremap.length(); 
		}
		if (index2 < 0) {
			index2 = scoremap.length();
		}
		
		return ScoreMatrix(index1 * 24 + index2);
	}
	
	/***
	** Other Experiments: Places
	**/
	
	private val root = GlobalRef[Project](this);
	
	// Places-compatible entry computation (disabled by default)
	public def computeEntryPlaces(i: Long, j: Long, gapOpen: Float, gapExtend: Float) {
		val dG = at (root) root().g;
		val dH = at (root) root().h;
		val dScoreMatrix = at (root) root().ScoreMatrix;
		val dMatrixFlat = at (root) root().matrixflat;
		val dSizesOfVerticalGaps = at (root) root().sizesOfVerticalGaps;
		val dSizesOfHorizontalGaps = at (root) root().sizesOfHorizontalGaps;
		val dPointers = at (root) root().pointers;
		val dS1 = at (root) root().s1arr;
		val dS2 = at (root) root().s2arr;
		
		var f : Float = 0f;
		val N = dS1.size + 1;
		val M = dS2.size + 1;
		val l = N * i + j; // id of this entry
		val mn = M + N;
		
		
		var temp1 : Float = dG(j) - gapExtend; // Top (Gotoh)
		var temp2 : Float = 0f;

		// Similarity compute
		var scoremap : String = "ARNDCQEGHILKMFPSTWYVBZX";
		var index1 : Long = scoremap.indexOf(dS1(i-1));
		var index2 : Long = scoremap.indexOf(dS2(j-1));
		
		// Wildcard entry
		if (index1 < 0) {
			index1 = scoremap.length(); 
		}
		if (index2 < 0) {
			index2 = scoremap.length();
		}
		var similarScore : Float = dScoreMatrix(index1 * 24 + index2);
		
		// End similarity compute
		
		f = dMatrixFlat(((i-1 + j-1)%3)*mn + (j-1)) + similarScore; // Diagonal
		temp2 = dMatrixFlat(((i-1 + j)%3)*mn + j) - gapOpen; // Top (Gotoh)

		// Top
		var resG : Float = 0;
		if (temp1 > temp2) {
			val resVar1 = temp1;
			val resVar2 = dSizesOfVerticalGaps(l-n) + 1us;
			at (root) {
				root().g(j) = resVar1;
				root().sizesOfVerticalGaps(l) = resVar2;
			}
			resG = temp1;
		} else {
			val resVar1 = temp2;
			at (root) {
				root().g(j) = resVar1;
			}
			resG = temp2;
		}
		
		// Left
		temp1 = dH(i) - gapExtend;
		temp2 = dMatrixFlat(((i + j-1)%3)*mn + (j-1)) - gapOpen;
		var resH : Float = 0;
		if (temp1 > temp2) {
			val resVar1 = temp1;
			val resVar2 = dSizesOfHorizontalGaps(l-1) + 1us;
			at (root) {
				root().h(i) = resVar1;
				root().sizesOfHorizontalGaps(l) = resVar2;
			}
			resH = temp1;
		} else {
			val resVar1 = temp2;
			at (root) {
				root().h(i) = resVar1;
			}
			resH = temp2;
		}
		
		// Find max among the values computed
		var result : Float = f;
		var direction : UByte = 1uy;
		if (resG > result) {
			result = resG;
			direction = 2uy;
		}
		if (resH > result) {
			result = resH;
			direction = 3uy;
		}
		if (0 >= result) {
			result = 0;
			direction = 0uy;
		}
		
		val resResult = result;
		val resPointer = direction;
		val longlen = at (root) root().longestlen;
		at (root) {
			root().matrixflat(((i + j)%3)*mn + j) = resResult;
			root().pointers(l) = resPointer;
		}
		// Set the starting cell for traceback
		if (result > longlen) {
			at (root) {
				root().longestlen = resResult;
				root().longestx = i;
				root().longesty = j;
			}
		}

		return result;
	}
	
	// Places computation (disabled by default)
	public def placesCompute() {
		s1arr = s1.chars();
		s2arr = s2.chars();
		
		// Start computation
		var extraMessage : String = "";
		var s1len : Long = s1.length();
		var s2len : Long = s2.length();
		
		if (s1len < minLength && s2len < minLength) {
			enableParallel = false;
			extraMessage = " (sequence too short)";
		}
	
		Console.OUT.println("Using Wavefront method");
		if (enableParallel) {
			extraMessage = extraMessage + ", Intervals: " + intervalLength;
			if (maxDivisions > 0) {
				extraMessage = extraMessage + ", Divisions: " + maxDivisions;
			}
		}
		Console.OUT.println("Parallel: " + enableParallel + extraMessage);
		var iterations: Long = s1.length() + s2.length() - 1; // Number of possible diagonals
		
		// Iteration data
		var startx : Long = 0;
		var starty : Long = 0;
		
		// Iterations
		for (i in 0..(iterations-1)) {
			// Find the starting coord of the diagonal, as well as length of diagonal
			var endy : Long = 0;
			if (i < s1len){
				startx = i;
				starty = 0;
				endy = startx;
			} else {
				startx = s1len-1;
				starty = i - startx;
				endy = starty + startx;
			}	
			
			if (endy >= s2len){ // Matrix is not square
				endy = s2len-1;
			}
			val diagonallen = endy - starty + 1;
			
			if (enableParallel) {
				// Begin places diagonal
				val gapOpen = A;
				val gapExtend = B;
				val beginx = startx;
				val beginy = starty;
				
				val divisions = Place.numPlaces();
				val interval = Math.floor(diagonallen as Float / divisions) as Int;
				
				if (interval > 1) {
					finish for (p in Place.places()) {
						at (p) async {
							var intervallen : Long = interval;
							if (p.id == divisions-1) {
								intervallen = diagonallen - p.id * interval;
							}
							
							var covered : Long = p.id * interval;
							for (c in 0..(intervallen-1)) {
								computeEntryPlaces(beginx-c+1-covered, beginy+c+1+covered, gapOpen, gapExtend);
							}
						}
					}
				} else {
					// One interval, just do sequential mode
					computeRange(startx, starty, diagonallen, false);
				}
				// End places diagonal
			} else {
				// Sequential version for short iterations
				computeRange(startx, starty, diagonallen, false);
			}
		}
		// End computation
	}
	
	/***
	** Other Experiments: CUDA
	**/
	
	val topo = PlaceTopology.getTopology();
	val gpu = topo.numChildren(here)==0 ? here : topo.getChild(here, 0);
	
	// CUDA computation (disabled by default)
	// Not tested as X10 was unable to compile CUDA kernel on Windows
	/*public def cudaCompute() {
		val dPointers = CUDAUtilities.makeGlobalRail[UByte](gpu, m * n);
		val dSizesOfVerticalGaps = CUDAUtilities.makeGlobalRail[UShort](gpu, m * n);
		val dSizesOfHorizontalGaps = CUDAUtilities.makeGlobalRail[UShort](gpu, m * n);
		val dMatrixFlat = CUDAUtilities.makeGlobalRail[Float](gpu, 3 * (m + n));
		val dG = CUDAUtilities.makeGlobalRail[Float](gpu, n);
		val dH = CUDAUtilities.makeGlobalRail[Float](gpu, m);
		val dS1 = CUDAUtilities.makeGlobalRail[Char](gpu, m);
		val dS2 = CUDAUtilities.makeGlobalRail[Char](gpu, n);
		val dScoreMatrix = CUDAUtilities.makeGlobalRail[Long](gpu, 24*24);
		s1arr = s1.chars();
		s2arr = s2.chars();
		finish {
			Rail.asyncCopy(sizesOfVerticalGaps, 0, dSizesOfVerticalGaps, 0, m * n);
			Rail.asyncCopy(sizesOfHorizontalGaps, 0, dSizesOfHorizontalGaps, 0, m * n);
			Rail.asyncCopy(g, 0, dG, 0, m);
			Rail.asyncCopy(h, 0, dH, 0, n);
			Rail.asyncCopy(s1arr, 0, dS1, 0, m-1);
			Rail.asyncCopy(s2arr, 0, dS2, 0, n-1);
			Rail.asyncCopy(ScoreMatrix, 0, dScoreMatrix, 0, 24*24);
		}
		
		// Start computation
		var extraMessage : String = "";
		var s1len : Long = s1.length();
		var s2len : Long = s2.length();
		
		if (s1len < minLength && s2len < minLength) {
			enableParallel = false;
			extraMessage = " (sequence too short)";
		}
	
		Console.OUT.println("Using Wavefront method");
		if (enableParallel) {
			extraMessage = extraMessage + ", Intervals: " + intervalLength;
			if (maxDivisions > 0) {
				extraMessage = extraMessage + ", Divisions: " + maxDivisions;
			}
		}
		Console.OUT.println("Parallel: " + enableParallel + extraMessage);
		var iterations: Long = s1.length() + s2.length() - 1; // Number of possible diagonals
		
		// Iteration data
		var startx : Long = 0;
		var starty : Long = 0;
		
		// Iterations
		for (it in 0..(iterations-1)) {
			// Find the starting coord of the diagonal, as well as length of diagonal
			var endy : Long = 0;
			if (it < s1len){
				startx = it;
				starty = 0;
				endy = startx;
			} else {
				startx = s1len-1;
				starty = it - startx;
				endy = starty + startx;
			}	
			
			if (endy >= s2len){ // Matrix is not square
				endy = s2len-1;
			}
			val diagonallen = endy - starty + 1;
			
			if (enableParallel) {
				// Begin diagonal
				val gapOpen = A;
				val gapExtend = B;
				val beginx = startx;
				val beginy = starty;
				
				val blocks = CUDAUtilities.autoBlocks();
				val threads = CUDAUtilities.autoThreads();
				val totalthreads = blocks * threads;
				val interval = Math.ceil(diagonallen as Float / totalthreads) as Long;
			
				val divisions = totalthreads;
				
				finish async at (gpu) @CUDA {
					finish for (block in 0n..(blocks-1n)) async {
						clocked finish for (thread in 0n..(threads-1n)) clocked async {
							val id = (block * threads + thread) as Long;
						
							var intervallen : Long = interval;
							if (id == divisions-1) {
								intervallen = diagonallen - id * interval;
							}
							
							var covered : Long = id * interval;
							for (c in 0..(intervallen-1)) {
								val i = beginx-c+1-covered;
								val j = beginy+c+1+covered;
								
								// Begin compute
								var f : Float = 0f;
								val N = dS1.size + 1;
								val M = dS2.size + 1;
								val l = N * i + j; // id of this entry
								val mn = M + N;
								
								
								var temp1 : Float = dG(j) - gapExtend; // Top (Gotoh)
								var temp2 : Float = 0f;

								// Similarity compute
								var scoremap : String = "ARNDCQEGHILKMFPSTWYVBZX";
								var index1 : Long = scoremap.indexOf(dS1(i-1));
								var index2 : Long = scoremap.indexOf(dS2(j-1));
								
								// Wildcard entry
								if (index1 < 0) {
									index1 = scoremap.length(); 
								}
								if (index2 < 0) {
									index2 = scoremap.length();
								}
								var similarScore : Float = dScoreMatrix(index1 * 24 + index2);
								
								// End similarity compute
								
								f = dMatrixFlat(((i-1 + j-1)%3)*mn + (j-1)) + similarScore; // Diagonal
								temp2 = dMatrixFlat(((i-1 + j)%3)*mn + j) - gapOpen; // Top (Gotoh)

								// Top
								var resG : Float = 0;
								if (temp1 > temp2) {
									val resVar1 = temp1;
									val resVar2 = dSizesOfVerticalGaps(l-n) + 1us;
									dG(j) = resVar1;
									dSizesOfVerticalGaps(l) = resVar2;
									resG = temp1;
								} else {
									val resVar1 = temp2;
									dG(j) = resVar1;
									resG = temp2;
								}
								
								// Left
								temp1 = dH(i) - gapExtend;
								temp2 = dMatrixFlat(((i + j-1)%3)*mn + (j-1)) - gapOpen;
								var resH : Float = 0;
								if (temp1 > temp2) {
									val resVar1 = temp1;
									val resVar2 = dSizesOfHorizontalGaps(l-1) + 1us;
									dH(i) = resVar1;
									dSizesOfHorizontalGaps(l) = resVar2;
									resH = temp1;
								} else {
									val resVar1 = temp2;
									dH(i) = resVar1;
									resH = temp2;
								}
								
								// Find max among the values computed
								var result : Float = f;
								var direction : UByte = 1uy;
								if (resG > result) {
									result = resG;
									direction = 2uy;
								}
								if (resH > result) {
									result = resH;
									direction = 3uy;
								}
								if (0 >= result) {
									result = 0;
									direction = 0uy;
								}
								
								val resResult = result;
								val resPointer = direction;
								val longlen = at (root) root().longestlen;
								at (root) {
									root().matrixflat(((i + j)%3)*mn + j) = resResult;
									root().pointers(l) = resPointer;
								}
								// Set the starting cell for traceback
								if (result > longlen) {
									at (root) {
										root().longestlen = resResult;
										root().longestx = i;
										root().longesty = j;
									}
								}
								// End compute
							}
						}
					}
				}
				// End diagonal
			} else {
				// Sequential version for short iterations
				computeRange(startx, starty, diagonallen, false);
			}
		}
		// End computation
		
		// Copy results
		finish {
			Rail.asyncCopy(dSizesOfVerticalGaps, 0, sizesOfVerticalGaps, 0, m * n);
			Rail.asyncCopy(dSizesOfHorizontalGaps, 0, sizesOfHorizontalGaps, 0, m * n);
			Rail.asyncCopy(dPointers, 0, pointers, 0, m * n);
		}
		
		// Delete globals
		CUDAUtilities.deleteGlobalRail(dPointers);
		CUDAUtilities.deleteGlobalRail(dSizesOfVerticalGaps);
		CUDAUtilities.deleteGlobalRail(dSizesOfHorizontalGaps);
		CUDAUtilities.deleteGlobalRail(dMatrixFlat);
		CUDAUtilities.deleteGlobalRail(dG);
		CUDAUtilities.deleteGlobalRail(dH);
		CUDAUtilities.deleteGlobalRail(dS1);
		CUDAUtilities.deleteGlobalRail(dS2);
		CUDAUtilities.deleteGlobalRail(dScoreMatrix);
	}*/
}