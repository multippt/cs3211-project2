<?php
/**
Usage: php benchcheckequal.php <program result> <jaligner result>
*/

function getJalignerPair($data) {
	$parts = explode("\n", $data);
	$i = 13;
	$sequence1 = "";
	$sequence2 = "";
	while ($i < count($parts)) {
		// Sequence 1
		$line = $parts[$i++];
		$line = preg_replace("(\s+)", " ", $line);
		$lineparts = explode(" ", $line);
		if (count($lineparts) > 2) {
			$sequence1 .= $lineparts[2];
		}
		
		$i++;
		
		// Sequence 2
		if ($i < count($parts)) {
			$line = $parts[$i++];
			$line = preg_replace("(\s+)", " ", $line);
			$lineparts = explode(" ", $line);
			if (count($lineparts) > 2) {
				$sequence2 .= $lineparts[2];
			}
		}
		
		$i++;
	}
	return array($sequence1, $sequence2);
}

function getProgramPair($data) {
	$parts = explode("\n", $data);
	$sequence1 = substr($parts[8], 3);
	$sequence2 = substr($parts[9], 3);
	return array($sequence1, $sequence2);
}

function comparePairs($data1,$data2) {
	$set1 = getProgramPair($data1);
	$set2 = getJalignerPair($data2);
	return $set1[0] == $set2[0] && $set1[1] == $set2[1];
}

function doCompare($path1, $path2) {
	return comparePairs(file_get_contents($path1), file_get_contents($path2));
}

echo json_encode(doCompare($argv[1], $argv[2]));
?>