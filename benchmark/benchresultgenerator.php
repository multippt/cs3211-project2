<?php

$showspeedup = true;
$sets = 50;

function getTiming($filedata) {
	$parts = explode("\n", $filedata);
	$timepart = $parts[4];
	$time = str_replace("Build Complete. Time taken: ", "", $timepart);
	$time = str_replace(" ms", "", $time);
	return $time;
}

echo "<table><tr>
<td>Sequence Length</td>
<td>Sequential</td>
<td>2 Processors</td>
<td>3 Processors</td>
<td>4 Processors</td></tr>";

for ($i = 1; $i <= $sets; $i++) {
	
	$n = "";
	if ($i < 10) {
		$n = "0";
	}
	$n .= $i;
	$n .= "00";
	
	echo "<tr><td>".$n."</td>";
	
	$resultname = "out".$n;
	$result_p1 = file_get_contents($resultname."-seq.txt");
	$result_p2 = file_get_contents($resultname."-parallel-2.txt");
	$result_p3 = file_get_contents($resultname."-parallel-3.txt");
	$result_p4 = file_get_contents($resultname."-parallel-4.txt");
	
	$times = array(getTiming($result_p1),getTiming($result_p2),getTiming($result_p3),getTiming($result_p4));
	
	echo "<td>".number_format($times[0],2)."</td>";
	if ($showspeedup) {
	echo "<td>".number_format($times[1],2)." (".number_format($times[0]/$times[1],2)."x)</td>";
	echo "<td>".number_format($times[2],2)." (".number_format($times[0]/$times[2],2)."x)</td>";
	echo "<td>".number_format($times[3],2)." (".number_format($times[0]/$times[3],2)."x)</td>";
	} else {
	echo "<td>".number_format($times[1],2)."</td>";
	echo "<td>".number_format($times[2],2)."</td>";
	echo "<td>".number_format($times[3],2)."</td>";	
	}
	echo "</tr>";
}
echo "</table>";
?>