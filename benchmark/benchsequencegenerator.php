<?php
/**
Usage: php benchsequencegenerator.php <size>
*/
function generate($length) {
	$result = ">random".$length."\n";
	$dictionary = "ACGT";
	$separator = 70;
	for ($i = 0; $i < $length; $i++) {
		if ($i > 0 && $i % 70 == 0) { // Line break
			$result .= "\n";
		}
		$randomInt = rand(0,3);
		$result .= $dictionary[$randomInt];
	}
	return $result;
}

if (isset($argv[1])) {
	echo generate($argv[1]);
} else {
	echo "Usage: php benchsequencegenerator.php <size>";
}
?>